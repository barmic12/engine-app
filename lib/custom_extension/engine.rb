module CustomExtension
  class Engine < ::Rails::Engine
    isolate_namespace CustomExtension

    config.generators do |g|
       g.test_framework :rspec
       g.fixture_replacement :factory_bot, dir: 'spec/factories'
       g.views false
       g.assets false
       g.helper false
     end
  end
end
