$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "custom_extension/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "custom_extension"
  s.version     = CustomExtension::VERSION
  s.authors     = ["Bartosz Michalak"]
  s.email       = ["barmic12@gmail.com"]
  s.homepage    = "https://git.crusar.eu"
  s.summary     = "Custom Externsion"
  s.description = "Example extension"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5.1.6", ">= 5.1.6.1"

  s.add_development_dependency "sqlite3"
  s.add_development_dependency 'rspec-rails'
  s.add_development_dependency 'factory_bot_rails'
  s.add_development_dependency 'rubocop'
  s.add_development_dependency 'brakeman'

  #s.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  #s.bindir        = "exe"
  #s.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  s.require_paths = ["lib"]
  s.test_files = Dir["spec/**/*"]
end
