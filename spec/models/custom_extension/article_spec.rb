require 'rails_helper'

module CustomExtension
  RSpec.describe Article, type: :model do
    it 'has a valid factory' do
      expect(build(:custom_extension_article)).to be_valid
    end
  end
end
