FactoryBot.define do
  factory :custom_extension_article, class: 'CustomExtension::Article' do
    title { 'Test title' }
    body { 'Test body' }
  end
end
