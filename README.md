# CustomExtension
This is demo of gem, which will be used in modular app.

## Usage
To list routes use this command:

```ruby
bundle exec rails app:routes
```

## Installation
Clone this repostitory to your computer.

After this, set bundle config:

```ruby
bundle config local.custom_extension /path/to/local/git/repository
```

where ``/path/to/local/git/repository`` is your local path, where you placed the gem repository.

Then add this to Gemfile:

```ruby
gem 'custom_extension', github: 'custom_extension/custom_extension', branch: 'master'
```


And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install custom_extension
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
