class CreateCustomExtensionComments < ActiveRecord::Migration[5.1]
  def change
    create_table :custom_extension_comments do |t|
      t.references :article, foreign_key: true
      t.text :body

      t.timestamps
    end
  end
end
